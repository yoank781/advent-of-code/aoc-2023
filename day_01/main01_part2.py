"""
--- Day 1: Trebuchet?! ---
...
--- Part Two ---
Your calculation isn't quite right. It looks like some of the digits are actually spelled out with letters: one, two, three, four, five, six, seven, eight, and nine also count as valid "digits".

Equipped with this new information, you now need to find the real first and last digit on each line. For example:

two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
In this example, the calibration values are 29, 83, 13, 24, 42, 14, and 76. Adding these together produces 281.

What is the sum of all of the calibration values?

Answer dataset01: ?????
"""

#one, two, three, four, five, six, seven, eight, and nine
def getDigitFromLitteral(tableau, index):
  digits = {
    "one" : 1, 
    "two" : 2,
    "three" : 3,
    "four" : 4,
    "five" : 5,
    "six" : 6,
    "seven" : 7,
    "eight" : 8,
    "nine" : 9
    }

  # les digits sont de taille 3, 4 ou 5
  # faire un sustring de 3, 4 ou 5 et chercher dans le dictionnaire
  print(f'recherche d''un digit dans le tableau à partir de l''index {index}')




print("Day 1: Trebuchet?!")

dataset = 'dataset02.txt'

somme = 0

with open(dataset) as f:
    for line in f:
        #print(line, end='\n')
        t = list(line)
        first = 0
        second = 0
        firstFound = False
        for index, char in t:
            #print(c, end=',')
            if (char.isdigit()):
                second = char
                if (not firstFound):
                    first = char
                    firstFound = True
            else:
                print(f'lettre : {c}')

        nombre = int(first + second)
        #print(f'nombre = {nombre}.')
        somme += nombre


print(f'Somme = {somme}.')
